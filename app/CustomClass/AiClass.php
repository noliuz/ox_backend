<?php
namespace App\CustomClass;

class AiClass {
  private $scoreArr = [];

  public function __construct() {
    //  $this->scoreArr = ['a'];
    return;
  }

  public function test() {
    print_r($this->scoreArr);
    exit();
  }

  public function evaluateGameScore($data_arr) {
    $oScore = $this->evaluateBySideGameScore($data_arr,'o');
    $xScore = $this->evaluateBySideGameScore($data_arr,'x');
    return $oScore-$xScore;
  }

  public function evaluateBySideGameScore($data_arr,$sideChar) {
      //vertical
    $score = 0;
    for ($x=0;$x<5;$x++) {
      $start_count = false;
      for ($y=0;$y<5;$y++) {
        if ($data_arr[$y][$x] == $sideChar && $start_count == false) {
          $start_count = true;
          continue;
        }
        if ($data_arr[$y][$x] == $sideChar && $start_count == true)
          ++$score;
        if ($data_arr[$y][$x] != $sideChar && $start_count == true)
          $start_count = false;
      }
    }
    $vertical_score = $score;
      //horizon
    $score = 0;
    for ($y=0;$y<5;$y++) {
      $start_count = false;
      for ($x=0;$x<5;$x++) {
        if ($data_arr[$y][$x] == $sideChar && $start_count == false) {
          $start_count = true;
          continue;
        }
        if ($data_arr[$y][$x] == $sideChar && $start_count == true)
          ++$score;
        if ($data_arr[$y][$x] != $sideChar && $start_count == true)
          $start_count = false;
      }
    }
    $horizon_score = $score;
      //north east
    $score = 0;
    for ($y_pos=1;$y_pos<=4;$y_pos++) {
      $y = $y_pos;
      $x = 0;
      $start_count = false;
      while ($y >= 0 && $x <= 4) {
        if ($data_arr[$y][$x] == $sideChar && $start_count == false) {
          $start_count = true;
          $y--;
          $x++;
          continue;
        }

        if ($data_arr[$y][$x] == $sideChar && $start_count == true)
          ++$score;
        if ($data_arr[$y][$x] != $sideChar && $start_count == true)
          $start_count = false;
        //print "$x $y <br>";
        $y--;
        $x++;

      }
    }
    $north_east_score = $score;
    //north west
    $score = 0;
    for ($y_pos=1;$y_pos<=4;$y_pos++) {
      $y = $y_pos;
      $x = 4;
      $start_count = false;
      while ($y >= 0 && $x >= 0) {
        if ($data_arr[$y][$x] == $sideChar && $start_count == false) {
          $start_count = true;
          $y--;
          $x--;
          continue;
        }

        if ($data_arr[$y][$x] == $sideChar && $start_count == true)
          ++$score;
        if ($data_arr[$y][$x] != $sideChar && $start_count == true)
          $start_count = false;
        //print "$x $y <br>";
        $y--;
        $x--;

      }
    }
    $north_west_score = $score;
      //x = 1..3 , y = 4 and north west/east
      //north west
    $score = 0;
    for ($x_pos=1;$x_pos<=3;$x_pos++) {
      $y = 4;
      $x = $x_pos;
      $start_count = false;
      while ($y >= 0 && $x >= 0) {
        if ($data_arr[$y][$x] == $sideChar && $start_count == false) {
          $start_count = true;
          $y--;
          $x--;
          continue;
        }

        if ($data_arr[$y][$x] == $sideChar && $start_count == true)
          ++$score;
        if ($data_arr[$y][$x] != $sideChar && $start_count == true)
          $start_count = false;
        $y--;
        $x--;
      }
    }

    //north east
    $score = 0;
    for ($x_pos=1;$x_pos<=3;$x_pos++) {
      $y = 4;
      $x = $x_pos;
      $start_count = false;
      while ($y >= 0 && $x <= 4) {
        if ($data_arr[$y][$x] == $sideChar && $start_count == false) {
          $start_count = true;
          $y--;
          $x++;
          continue;
        }

        if ($data_arr[$y][$x] == $sideChar && $start_count == true)
          ++$score;
        if ($data_arr[$y][$x] != $sideChar && $start_count == true)
          $start_count = false;
        $y--;
        $x++;
      }
    }
    $north_and_west_extra_score = $score;
    return $vertical_score+$horizon_score+$north_east_score+$north_west_score+$north_and_west_extra_score;
  }

  public function minimax($boardArr,$lastSideChar,$moveArr,$depth,&$sumScore,$maxDepth,&$bestScore) {
    set_time_limit(300); //php set 5 minutes
    $dataHelper = new DataHelperClass();
    $ai = new AiClass();
      //debug
    //$dataHelper->printBoardAsHtml($boardArr);
      ///
    $score = $ai->evaluateGameScore($boardArr);
    $sumScore += $score;

    if ($depth == 1) {
      $y = $moveArr['y'];
      $x = $moveArr['x'];

      array_push($this->scoreArr,[$y,$x]);
    }

    if ($depth == $maxDepth || $dataHelper->isGameOver($boardArr)) {
      if ($sumScore > $bestScore)
        $bestScore = $sumScore;

      array_push($this->scoreArr,$sumScore);

      //print "depth $depth ,sum $sumScore ,best $bestScore ";
      //print_r($moveArr);
      //print "<br>";
      $sumScore = 0;
      return;
    }

    //find all valid moves
    for ($y=0;$y<5;$y++) {
      for ($x=0;$x<5;$x++) {
        if ($boardArr[$y][$x] == '_') {//found valid move
          if ($lastSideChar == 'x')
            $sideChar = 'o';
          else {
            $sideChar = 'x';
          }

          $dummyBoardArr = $boardArr;
          $dummyBoardArr[$y][$x] = $sideChar;

          //minimax($boardArr,$lastSideChar,$depth,$sumScore,$maxDepth,$nodeCount)
          $moveArr['y'] = $y;
          $moveArr['x'] = $x;
          $this->minimax($dummyBoardArr,$sideChar,$moveArr,$depth+1,$sumScore,$maxDepth,$bestScore);

        }
      }
    }
  }

  public function chooseBestMove($boardArr,$maxDepth) {
    $bestScore = -999999;
    $sumScore = 0;
    $this->minimax($boardArr,'x',[],0,$sumScore,$maxDepth,$bestScore);
    $scoreArr = $this->getScoreArr();

    //$dataHelper = new DataHelperClass();
    //$dataHelper->dumpArr($scoreArr);
    //exit();

    //get array index
    $arrIndex = [];
    for ($i=0;$i<count($scoreArr);$i++) {
      if (is_array($scoreArr[$i]))
        array_push($arrIndex,$i);
    }
    //get all moves with best score
    $bestMoveArr = [];
    for ($i=0;$i<count($arrIndex);$i++) {
      if ($i == count($arrIndex)-1)
        break;

      $startScoreIndex = $arrIndex[$i]+1;
      $length = $arrIndex[$i+1] - $arrIndex[$i] - 1;
      //print "$startScoreIndex $length <br>";

      $move = [];
      $move['move'] = $scoreArr[$arrIndex[$i]];
      $move['bestScore'] = max(array_slice($scoreArr,$startScoreIndex,$length));
      if ($move['bestScore'] == $bestScore)
        array_push($bestMoveArr,$move);
    }

    return $bestMoveArr;

  }

  public function getScoreArr() {
    return $this->scoreArr;
  }
}

?>
