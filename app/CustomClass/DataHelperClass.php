<?php
namespace App\CustomClass;

class DataHelperClass {
  public function convertStr2BoardArr($str) {
    $data_arr = [];
    for ($x=0;$x<5;$x++) {
      for ($y=0;$y<5;$y++) {
        $data_arr[$y][$x] = $str[$y*5+$x];
      }
    }

    return $data_arr;
  }

  public function isGameOver($boardArr) {
    if ($this->isBoardFull($boardArr))
      return true;
    else if ($this->isWin($boardArr,'o'))
      return true;
    else if ($this->isWin($boardArr,'x'))
      return true;

    return false;
  }

  public function isBoardFull($boardArr) {
    //check fulling of moves in board
    for ($y=0;$y<5;$y++) {
      for ($x=0;$x<5;$x++) {
        if ($boardArr[$y][$x] == '_')
          return false;
      }
    }

    return true;
  }

  public function isWin($boardArr,$sideChar) {
    //horizontal
    for ($y=0;$y<5;$y++) {
      for ($x=0;$x<5;$x++) {
        if ($boardArr[$y][$x] == $sideChar) {
          if ($x == 4)
            return true;
        } else {
          break;
        }
      }
    }

    //vertical
    for ($x=0;$x<5;$x++) {
      for ($y=0;$y<5;$y++) {
        if ($boardArr[$y][$x] == $sideChar) {
          if ($y == 4)
            return true;
        } else {
          break;
        }
      }
    }

    //north west
    $x=4;$y=4;
    while ($x>=0 && $y >= 0) {
      if ($boardArr[$y][$x] != $sideChar) {
        break;
      } else {
        if ($y == 0 && $x == 0)
        return true;
      }
      $x--;
      $y--;
    }

    //north east
    $x=0;$y=4;
    while ($x<=4 && $y >= 0) {
      if ($boardArr[$y][$x] != $sideChar) {
        break;
      } else {
        if ($y == 0 && $x == 4)
          return true;
      }
      $x++;
      $y--;
    }

    return false;

  }

  public function printBoardAsHtml($data_arr) {
    print '<table border=1>';
    for ($row=0;$row<5;$row++) {
      print '<tr>';
      print '<td>'.$data_arr[$row][0].'</td>';
      print '<td>'.$data_arr[$row][1].'</td>';
      print '<td>'.$data_arr[$row][2].'</td>';
      print '<td>'.$data_arr[$row][3].'</td>';
      print '<td>'.$data_arr[$row][4].'</td>';
      print '</tr> '."\n";
    }
    print '</table>';

    return;
  }

  public function dumpArr($arr) {
    print '<pre>';
    var_dump($arr);
    print '</pre>';
  }

}

?>
