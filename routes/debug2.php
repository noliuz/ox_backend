<?php
use App\CustomClass\AiClass;

function minimaxForOMove($boardArr,$depth,$sumScore,$maxDepth,$nodeCount) {
  //print "nodeCount = $nodeCount<br>";
  //$nodeCount++;
  print "$sumScore <br>";

  if ($depth == $maxDepth)
    return $sumScore;

  $dataHelper = new DataHelperClass();
  if ($dataHelper->isGameOver($boardArr))
    return $sumScore;

  $ai = new AiClass();

  $score = $ai->evaluateGameScore($boardArr);
  $sumScore += $score;

  //find all valid moves
  for ($y=0;$y<5;$y++) {
    for ($x=0;$x<5;$x++) {
      if ($boardArr[$y][$x] == '_') {//found valid move
        $dummyBoardArr = $boardArr;
        $dummyBoardArr[$y][$x] = 'o';

        // minimaxForXMove($boardArr,$depth,$sumScore,$maxDepth,$nodeCount)

        if ($depth < $maxDepth) {
          $score = minimaxForXMove($dummyBoardArr,$depth+1,$sumScore,$maxDepth,$nodeCount);
        }
      }
    }
  }
}

function minimaxForXMove($boardArr,$depth,$sumScore,$maxDepth,$nodeCount) {
  //print "nodeCount = $nodeCount<br>";
  //$nodeCount++;

  if ($depth == $maxDepth)
    return $sumScore;

  $dataHelper = new DataHelperClass();
  if ($dataHelper->isGameOver($boardArr))
    return $sumScore;

  $ai = new AiClass();

  $score = $ai->evaluateGameScore($boardArr);
  $sumScore += $score;
  print "$sumScore <br>";

  //find all valid moves
  for ($y=0;$y<5;$y++) {
    for ($x=0;$x<5;$x++) {
      if ($boardArr[$y][$x] == '_') {//found valid move
        $dummyBoardArr = $boardArr;
        $dummyBoardArr[$y][$x] = 'x';

        if ($depth < $maxDepth) {
          $score = minimaxForOMove($dummyBoardArr,$depth+1,$sumScore,$maxDepth,$nodeCount);
        }
      }
    }
  }
}


?>
