<?php

use Illuminate\Http\Request;
use App\CustomClass\AiClass;
use App\CustomClass\DataHelperClass;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/{data}', function ($data) {
  //$data = 'oxoxo____________________';
  if (strlen($data) != 25)
    return 'data error';

  $dataHelper = new DataHelperClass();
  $ai = new AiClass();
  $boardArr = $dataHelper->convertStr2BoardArr($data);
  //debug
  //$dataHelper->printBoardAsHtml($boardArr);
  ////

  $bestMoveArr = $ai->chooseBestMove($boardArr,2);
  //$dataHelper->dumpArr($bestMoveArr);
  //exit();

  //random move if there are more than best one
  if (count($bestMoveArr) > 1) {
    $chooseIndex = rand(0,count($bestMoveArr)-1);
  }
  //print_r($bestMoveArr[$chooseIndex]);
  $res['y'] = $bestMoveArr[$chooseIndex]['move'][0];
  $res['x'] = $bestMoveArr[$chooseIndex]['move'][1];

  return json_encode($res);
});
